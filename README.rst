Deploying
-------------
Obtain an `Imgur API key <http://api.imgur.com/>`_, then clone and configure:

.. sourcecode :: bash

    export IMGUR_CLIENT_ID=YOUR_API_KEY
    git clone git@bitbucket.org:dbarlett/randomimage.git
    virtualenv randomimage
    cd randomimage
    source bin/activate
    pip install -r requirements.txt

Testing
-------

.. sourcecode :: bash

    python wsgi.py

Browse to http://127.0.0.1/imgur?album_id=YOUR_ALBUM_ID, and you will be redirected (HTTP 302) to a random image within the album.
