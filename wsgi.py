"""
Redirect to a random image in a given Imgur album.

"""

__title__ = 'randomimage'
__version__ = '1.1.0'
__author__ = 'Dylan Barlett'
__license__ = 'GPLv2'
__copyright__ = 'Copyright 2013 Dylan Barlett'

import os
import sys
import requests
from random import choice
from flask import Flask, request, redirect, abort

application = app = Flask(__name__)


def imgur_get_album_image_urls(album_id):
    """Retrieve URLs for all images in an Imgur album
    """
    url_base = "https://api.imgur.com/3/"
    client_id = os.environ["IMGUR_CLIENT_ID"]
    headers = {"Authorization": "Client-ID " + client_id}
    image_urls = []

    if (album_id is None) or (len(album_id) < 0):
        return image_urls

    r = requests.get(url_base + "album/" + album_id,
                     headers=headers,
                     verify=True)
    r.raise_for_status()
    for image in r.json()["data"]["images"]:
        try:
            image_urls.append(image["link"])
        except KeyError:
            pass
    return image_urls


@app.route("/")
def index():
    return "/imgur?album=ALBUM_ID"


@app.route("/imgur")
def imgur_get_random_image():
    if request.args.get("album"):
        image_urls = imgur_get_album_image_urls(request.args.get("album"))
        return redirect(choice(image_urls))
    else:
        return "/imgur?album=ALBUM_ID"


if __name__ == "__main__":
    app.run(debug=False)

